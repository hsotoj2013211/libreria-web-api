﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LibraryWebAPI.Models
{
    public class Book
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public int Year { get; set; }
        public decimal Price { get; set; }

        //FOREIGN KEY Autor
        public int AuthorId { get; set; }

        public Author Author { get; set; }

        //FOREIGN KEY Category
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}