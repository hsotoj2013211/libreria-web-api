﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LibraryWebAPI.Models
{
    public class Author
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string BirthDay { get; set; }
        public string Description { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}
