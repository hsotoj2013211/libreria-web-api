﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryWebAPI.Models
{
    public class AuthorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}