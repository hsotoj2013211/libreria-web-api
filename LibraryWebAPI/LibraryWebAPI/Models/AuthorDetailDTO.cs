﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryWebAPI.Models
{
    public class AuthorDetailDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BirthDay { get; set; }
        public string Description { get; set; }
    }
}