﻿var ViewModel = function () {
    var self = this;
    self.books = ko.observableArray();
    self.error = ko.observable();
    self.loading = ko.observable();
    self.loading1 = ko.observable();
    self.loading2 = ko.observable();
    self.detail = ko.observable();
    self.detailAuthor = ko.observable();
    self.detailCategory = ko.observable();
    self.authors = ko.observableArray();
    self.categories = ko.observableArray();

    var bookUri = '/api/Books';
    var authorUri = '/api/Authors';
    var categoryUri = '/api/Categories';

    function ajaxHelper(uri, method, data) {
        self.error = '';
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'applicaction/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }
    function getAllAuthors() {
        ajaxHelper(authorUri, 'GET').done(function (data) {
            self.authors(data);
            self.loading1(false);
        });
    }
    function getAllCategories() {
        ajaxHelper(categoryUri, 'GET').done(function (data) {
            self.categories(data);
            self.loading2(false);
        });

    };
    function getAllBooks() {
        ajaxHelper(bookUri, 'GET').done(function (data) {
            self.books(data);
            self.loading(false);
        });
    }

    self.getBookDetails = function (item) {
        ajaxHelper(bookUri + "/"+ item.Id, 'GET').done(function (data) {
            self.detail(data);
        });
    };

    self.getAuthorDetails = function (item) {
        ajaxHelper(authorUri + "/"+ item.Id, 'GET').done(function(data){
            self.detailAuthor(data);
        });
    };
    self.getCategoryDetails = function (item) {
        ajaxHelper(categoryUri + "/" + item.Id, 'GET').done(function (data) {
            self.detailCategory(data);
        });
    };

    self.newBook = {
        Author: ko.observable(),
        Category: ko.observable(),
        Price: ko.observable(),
        Year: ko.observable(),
        Title: ko.observable()

    };

    self.addBook = function (formElement) {
        var book = {
            AuthorId: self.newBook.Author().Id,
            CategoryId: self.newBook.Category().Id,
            Price: self.newBook.Price(),
            Title: self.newBook.Title(),
            Year: self.newBook.Year()
        }

        ajaxHelper(bookUri, 'POST', book).done(function (item) {
            self.books.push(item);
        });
    };

    getAllBooks();
    getAllAuthors();
    getAllCategories();
    self.loading(true);
    self.loading1(true);
};
ko.applyBindings(new ViewModel());