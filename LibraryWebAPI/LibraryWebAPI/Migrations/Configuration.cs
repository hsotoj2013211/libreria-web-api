namespace LibraryWebAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using LibraryWebAPI.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<LibraryWebAPI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LibraryWebAPI.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Authors.AddOrUpdate( x => x.Id, 
	            new Author() {Id=1, Name="Julio Verne", BirthDay="08/02/1888", Description="Este autor escribe libros de aventura."},
	            new Author() {Id=2, Name="Juan Perez" , BirthDay="01/01/1978", Description="Este autor escribe libros de Programacion."},
	            new Author() {Id=3, Name="Pablo Perez", BirthDay="02/02/1979", Description="Este autor escribe libros de Programacion."}
            );
            context.Categories.AddOrUpdate( x=> x.Id,
	            new Category() {Id=1, Name="Aventura", Description="Este genero se trata de sucesos extraordinarios que le sucede a una i a un grupo de personas."},
	            new Category() {Id=2, Name="Programacion", Description="Casi siempre los libros de este genero son ejemplos de distintos lenguajes de programacion"} 
            );
            context.Books.AddOrUpdate( x=>x.Id,
	            new Book() {Id=1, Title="El Faro Del Fin Del Mundo", Year=1903, Price=50.75M, AuthorId=1, CategoryId=1},
	            new Book() {Id=1, Title="Aprendiendo JAVA", Year=2005, Price=56.50M, AuthorId=2, CategoryId=2},
	            new Book() {Id=1, Title="Visual Basic para Principiantes", Year=2009, Price=60.80M, AuthorId=3, CategoryId=2}
            );
        }
    }
}
